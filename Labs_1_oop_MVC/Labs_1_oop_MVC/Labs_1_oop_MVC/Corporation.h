#pragma once
#include "Employees.h"
#include"Machines.h"

class Corporation // ������ ������ 
{
public: 
	Corporation(Employees &_employees, Machines &_machines);
	Corporation();
	~Corporation();

	std::string GetEmpName(int i);
	int GetEmpSalary(int i);
	std::string GetEmpProfession(int i);
	int GetEmpAge(int i);
	
	int SumSalary();
	void AddEmploy(int,std::string,int,std::string,int);
	void RemoveEmploy(int);

private:
	
	Machines machines; 
	int sum;
	Employees employees[10] = { Employees("Sasha",3500,"Loader",35), Employees("Pasha",2000,"Joiner",45) };
};

