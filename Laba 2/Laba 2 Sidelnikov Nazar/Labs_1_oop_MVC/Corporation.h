#pragma once
#include<iostream>
#include "Employees.h"
#include"Computers.h"

class Corporation 
{
public:
	Corporation (Employees &_employees, Computers &_computers);
	Corporation ();
	~Corporation ();

	std::string GetEmpName(int i);
	int GetEmpSalary(int i);
	std::string GetEmpProfession(int i);
	int GetEmpAge(int i);
	
	int SumSalary();
	void AddEmploy(int,std::string,int,std::string,int);
	void RemoveEmploy(int);

	bool GetWork(int);
	int GetNumber(int);

	void SetWork(int, bool);
	void SetNumber(int, int);
	void SwapEmp(int, int);
	void SortName();
	void SortProfession();
	void SortSalary();
	void SortAge();

private:
	
	Computers computers;
	int sum;
	Employees employees[10] =  { Employees("James",1500,"Porter",34),Employees("Natasha",3000,"Concreter",54),Employees("Polina",2650,"Driver",36),Employees("Nastya",12000,"Programmer",26) };
};

