#pragma once
#include<iostream>
#include "Corporation.h"
class View
{
public:
	View();
	~View();
	std::string Employ; 
	std::string EmpName;
	std::string EmpSalary;
	std::string EmpProfession;
	std::string EmpAge;
	std::string SumSalary;
	std::string EnterName;
	std::string EnterSalary;
	std::string EnterProfession;
	std::string EnterAge;
	std::string LayEmploy;

	void StartDisplay();
	
	void Status(std::string, std::string);
	void Status(std::string, int);
	void Status(std::string);
	void Status();  
};

