#pragma once

#include<string>
class Employees
{
public:
	Employees(std::string,int,std::string,int);
	Employees();
	~Employees();
	std::string GetName();
	int GetSalary();
	std::string GetProfession();
	int GetAge();

	void SetName(std::string);
	void SetSalary(int);
	void SetProfession(std::string);
	void SetAge(int);

private:
	std::string Name;
	int Salary;
	std::string Profession;
	int Age;
};

