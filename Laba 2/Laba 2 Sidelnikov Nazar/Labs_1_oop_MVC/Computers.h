#pragma once
#include<string>
class Computers
{
public:
	Computers();
	~Computers();
	int GetCost();
	int GetNumber(int);
	bool GetWork(int);

	void SetNumber(int,int);
	void SetWork(int, bool);

private:
	int Cost;
	bool *Work;
	int *Number;
	int arr;
};

