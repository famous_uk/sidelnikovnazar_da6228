#include "Computers.h"



Computers::Computers()
{
	arr = 5;
	Number = new int[arr];
	Work = new bool[arr];
	for (int a=0; a < 6; a++)
	{
		
		Work[a] = false;
	}
}


Computers::~Computers()
{
}

int Computers::GetCost()
{
	return Cost;
}

int Computers::GetNumber(int i)
{
	return Number[i];
}

bool Computers::GetWork(int i)
{
	return Work[i];
}

void Computers::SetNumber(int i, int number)
{
	Number[i] = number;
}

void Computers::SetWork(int i, bool work)
{
	Work[i] = work;
}
