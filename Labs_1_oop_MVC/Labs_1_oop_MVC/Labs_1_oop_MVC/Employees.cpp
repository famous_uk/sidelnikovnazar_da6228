#include "Employees.h"



Employees::Employees(std::string name, int salary, std::string profession, int age)
{
	this->Name = name;
	this->Salary = salary;
	this->Profession = profession;
	this->Age = age;
}

Employees::Employees()
{
	
}

Employees::~Employees()
{
}

std::string Employees::GetName()
{
	return Name;
}

int Employees::GetSalary()
{
	return Salary;
}

std::string Employees::GetProfession()
{
	return Profession;
}

int Employees::GetAge()
{
	return Age;
}

void Employees::SetName(std::string name)
{
	this->Name = name;
}

void Employees::SetSalary(int salary)
{
	this->Salary = salary;
}

void Employees::SetProfession(std::string profession)
{
	this->Profession = profession;
}

void Employees::SetAge(int age)
{
	this->Age = age;
}
