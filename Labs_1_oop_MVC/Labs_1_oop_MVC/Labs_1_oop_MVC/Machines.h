#pragma once
#include<string>
class Machines
{
public:
	Machines();
	~Machines();
	std::string GetName();
	int GetCost();
	int GetNumber();


private:
	std::string Name;
	int Cost;
	int Number;
};

