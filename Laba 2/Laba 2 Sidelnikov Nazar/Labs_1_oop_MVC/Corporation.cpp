#include "Corporation.h"

Corporation ::Corporation (Employees &_employees, Computers &_computers)
{
	*employees = _employees;
	computers = _computers;
	
}

Corporation ::Corporation ()
{
	

}

Corporation ::~Corporation ()
{


}

std::string Corporation ::GetEmpName(int i)
{
	return employees[i].GetName();
}

int Corporation ::GetEmpSalary(int i)
{
	return employees[i].GetSalary();
}

std::string Corporation ::GetEmpProfession(int i)
{
	return employees[i].GetProfession();
}

int Corporation ::GetEmpAge(int i)
{
	return employees[i].GetAge();
}

int Corporation ::SumSalary()
{
	sum = 0;
	for (int num = 0; employees[num].GetName() != ""; num++) 
		sum += employees[num].GetSalary();
		return sum;
}

void Corporation ::AddEmploy(int a,std::string b, int c, std::string d, int e)
{
	employees[a].SetName(b);
	employees[a].SetSalary(c);
	employees[a].SetProfession(d);
	employees[a].SetAge(e);
}

void Corporation ::RemoveEmploy(int a)
{
	do 
	{
		employees[a-1].SetName(employees[a].GetName());
		employees[a-1].SetSalary(employees[a].GetSalary());
		employees[a-1].SetProfession(employees[a].GetProfession());
		employees[a-1].SetAge(employees[a].GetAge());
		a++;
	} while (employees[a-1].GetName() != "");
}

bool Corporation ::GetWork(int i)
{
	return computers.GetWork(i);
}

int Corporation ::GetNumber(int i)
{
	return computers.GetNumber(i);
}

void Corporation ::SetWork(int a, bool b)
{
	computers.SetWork(a, b);
}

void Corporation ::SetNumber(int a, int b)
{
	computers.SetNumber(a, b);
}
void Corporation ::SwapEmp(int a, int b)
{
	std::swap(employees[a], employees[b]);
	
}



void Corporation ::SortName()
{
	for (int a = 0; employees[a+1].GetName()!=""; a++) {
		for (int b = a+1;employees[b].GetName()!= ""; b++) {
			
			if (strcmp(employees[a].GetName().c_str(), employees[b].GetName().c_str()) > 0)
			{
				SwapEmp(a, b);
			}
		}
	}
}

void Corporation ::SortProfession()
{
	for (int a = 0; employees[a + 1].GetName() != ""; a++) {
		for (int b = a + 1; employees[b].GetName() != ""; b++) {

			if (strcmp(employees[a].GetProfession().c_str(), employees[b].GetProfession().c_str()) > 0)
			{
				SwapEmp(a, b);
			}
		}
	}
}

void Corporation ::SortSalary()
{
	for (int a = 0; employees[a + 1].GetName() != ""; a++) {
		for (int b = a + 1; employees[b].GetName() != ""; b++) {

			if (employees[a].GetSalary()>employees[b].GetSalary())
			{
				SwapEmp(a, b);
			}
		}
	}
}

void Corporation ::SortAge()
{
	for (int a = 0; employees[a + 1].GetName() != ""; a++) {
		for (int b = a + 1; employees[b].GetName() != ""; b++) {

			if (employees[a].GetAge()>employees[b].GetAge())
			{
				SwapEmp(a, b);
			}
		}
	}
}


